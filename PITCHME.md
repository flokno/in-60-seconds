## Strongly Anharmonic Solids
#### From Screening to Accurate Thermal Conductivities

<br/>
Florian Knoop<br/>
@size[.75em](Thomas Purcell, Matthias Scheffler, Christian Carbogno)

@snap[north-west span-40]
![](assets/img/FHIlogo.png)
@snapend

@snap[south span-100]
@size[.75em](DPG Frühjahrstagung Regensburg – MM 37.9)
@snapend

---
@snap[north span-100]
### Thermal Conductivity `$ \kappa $`
@snapend

---
@snap[north span-100]
### Thermal Conductivity `$ \kappa $`
@snapend

@ul[west span-55](false)
- **Thermal management**
- Thermoelectrics
- Thermal barrier coatings
@ulend

@img[east span-40 shadow](assets/img/heatsink.jpg)

---
@snap[north span-100]
### Thermal Conductivity `$ \kappa $`
@snapend

@ul[west span-55](false)
- Thermal management
- **Thermoelectrics**
- Thermal barrier coatings
@ulend

@img[east span-40 shadow](assets/img/thermoel.jpg)

---
@snap[north span-100]
### Thermal Conductivity `$ \kappa $`
@snapend

@ul[west span-55](false)
- Thermal management
- Thermoelectrics
- **Thermal barrier coatings**
@ulend

@img[east span-40 shadow](assets/img/turbine.jpg)

@snap[south span-100 fragment]
@box[grey](**Less** than 1000 materials investigated in detail)
@snapend

---

## Add Some Slide Candy

![](assets/img/presentation.png)

---
@title[Customize Slide Layout]

@snap[west span-50]
## Customize Slide Content Layout
@snapend

@snap[east span-50]
![](assets/img/presentation.png)
@snapend

---?color=#E58537
@title[Add A Little Imagination]

@snap[north-west]
#### Add a splash of @color[cyan](**color**) and you are ready to start presenting...
@snapend

@snap[west span-55]
@ul[spaced text-white]
- You will be amazed
- What you can achieve
- *With a little imagination...*
- And **GitPitch Markdown**
@ulend
@snapend

@snap[east span-45]
@img[shadow](assets/img/conference.png)
@snapend

---?image=assets/img/presenter.jpg

@snap[north span-100 headline]
## Now It's Your Turn
@snapend

@snap[south span-100 text-06]
[Click here to jump straight into the interactive feature guides in the GitPitch Docs @fa[external-link]](https://gitpitch.com/docs/getting-started/tutorial/)
@snapend
